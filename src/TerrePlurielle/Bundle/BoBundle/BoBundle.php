<?php

namespace TerrePlurielle\Bundle\BoBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class BoBundle extends Bundle
{
    public function getParent()
    {
        return 'ApplicationSonataUserBundle';
    }
}