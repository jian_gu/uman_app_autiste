<?php

namespace TerrePlurielle\Bundle\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ActiviteAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('titre', 'text', array('label' => 'Titre'))
            ->add('dateDebut', 'date',array('label' => 'Date de début'), array('input'  => 'datetime', 'widget' => 'choice',))
            ->add('dateFin', 'date', array('label' => 'Date de fin'), array('input'  => 'datetime', 'widget' => 'choice',))
            ->add('statut', 'checkbox', array( 'label'     => 'Activité fini ?', 'required'  => false, ))
            ->add('refPictogramme', 'entity', array('label' => 'Pictogramme référent', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Pictogramme'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('titre')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('titre')
        ;
    }
}