<?php

namespace TerrePlurielle\Bundle\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class EnfantAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('nom', 'text')
            ->add('prenom', 'text')
            ->add('dateDeNaissance', 'date',array('label' => 'Date de début'), array('input'  => 'datetime', 'widget' => 'choice',))
            ->add('observation', 'text')
            ->add('refNiveau', 'entity', array('label' => 'Niveau référent', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Enfant', 'required' => false))
            ->add('refPictogramme', 'entity', array('label' => 'Pictogramme référent', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Enfant', 'required' => false))
            ->add('refParent', 'entity', array('label' => 'Parent référent', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Enfant', 'required' => false))
            ->add('refEducateur', 'entity', array('label' => 'Educateur référent', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Enfant', 'required' => false))
            ->add('refPhraseComposee', 'sonata_type_collection', array('label' => 'Phrase composee'))
            ->add('refExercice', 'sonata_type_collection', array('label' => 'Exercice'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nom')
            ->add('prenom')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('nom')
            ->addIdentifier('prenom')
        ;
    }
}