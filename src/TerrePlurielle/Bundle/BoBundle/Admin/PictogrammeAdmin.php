<?php

namespace TerrePlurielle\Bundle\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PictogrammeAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('pictoUrl', 'text', array('label' => 'Chemin du pictogramme'))
            ->add('refMot', 'entity', array('label' => 'Mot référent', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Mot'))
            ->add('refPhrase', 'entity', array('label' => 'Phrase référent', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\PhraseProposee'))
            ->add('refTheme', 'entity', array('label' => 'Thème', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Theme'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('refMot')
            ->add('pictoUrl')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('refMot')
            ->addIdentifier('pictoUrl')
        ;
    }
}