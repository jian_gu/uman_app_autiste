<?php

namespace TerrePlurielle\Bundle\FoBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Enfant_PhraseComposeesRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class Enfant_PhraseComposeesRepository extends EntityRepository
{
}
