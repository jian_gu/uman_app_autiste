<?php

namespace TerrePlurielle\Bundle\FoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Enfant
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TerrePlurielle\Bundle\FoBundle\Entity\EnfantRepository")
 */
class Enfant
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="Prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateDeNaissance", type="datetime")
     */
    private $dateDeNaissance;

    /**
     * @var string
     *
     * @ORM\Column(name="Observation", type="text")
     */
    private $observation;

    /**
     * @var string
     *
     * @ORM\Column(name="RefNiveau", type="string", length=255)
     */
    private $refNiveau;

    /**
     * @var string
     *
     * @ORM\Column(name="RefPictogramme", type="string", length=255)
     */
    private $refPictogramme;

    /**
     * @var string
     *
     * @ORM\Column(name="RefParent", type="string", length=255)
     */
    private $refParent;

    /**
     * @var string
     *
     * @ORM\Column(name="RefEducateur", type="string", length=255)
     */
    private $refEducateur;


    /**
     * 
     * @ORM\ManyToMany(targetEntity="PhraseComposee",cascade={"persist", "remove"})
     * @ORM\JoinTable(name="Enfant_PhraseComposees",
     *      joinColumns={@ORM\JoinColumn(name="EnfantId", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="PhraseComposeeId", referencedColumnName="id",unique=true)}
     *      )
     */
    private $refPhraseComposee;

    /**
     * 
     * @ORM\ManyToMany(targetEntity="Exercices",cascade={"persist", "remove"})
     * @ORM\JoinTable(name="Enfant_Exercices",
     *      joinColumns={@ORM\JoinColumn(name="EnfantId", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="ExerciceId", referencedColumnName="id",unique=true)}
     *      )
     */
    private $refExercice;

    public function __construct()
    {
        $this->refExercice = new \Doctrine\Common\Collections\ArrayCollection();
        $this->refPhraseComposee = new \Doctrine\Common\Collections\ArrayCollection();
    }

    
    public function __toString()
    {
        return $this->nom." ".$this->prenom;
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Enfant
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Enfant
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set dateDeNaissance
     *
     * @param \DateTime $dateDeNaissance
     * @return Enfant
     */
    public function setDateDeNaissance($dateDeNaissance)
    {
        $this->dateDeNaissance = $dateDeNaissance;

        return $this;
    }

    /**
     * Get dateDeNaissance
     *
     * @return \DateTime 
     */
    public function getDateDeNaissance()
    {
        return $this->dateDeNaissance;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return Enfant
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set refNiveau
     *
     * @param string $refNiveau
     * @return Enfant
     */
    public function setRefNiveau($refNiveau)
    {
        $this->refNiveau = $refNiveau;

        return $this;
    }

    /**
     * Get refNiveau
     *
     * @return string 
     */
    public function getRefNiveau()
    {
        return $this->refNiveau;
    }

    /**
     * Set refPictogramme
     *
     * @param string $refPictogramme
     * @return Enfant
     */
    public function setRefPictogramme($refPictogramme)
    {
        $this->refPictogramme = $refPictogramme;

        return $this;
    }

    /**
     * Get refPictogramme
     *
     * @return string 
     */
    public function getRefPictogramme()
    {
        return $this->refPictogramme;
    }

    /**
     * Set refParent
     *
     * @param string $refParent
     * @return Enfant
     */
    public function setRefParent($refParent)
    {
        $this->refParent = $refParent;

        return $this;
    }

    /**
     * Get refParent
     *
     * @return string 
     */
    public function getRefParent()
    {
        return $this->refParent;
    }

    /**
     * Set refEducateur
     *
     * @param string $refEducateur
     * @return Enfant
     */
    public function setRefEducateur($refEducateur)
    {
        $this->refEducateur = $refEducateur;

        return $this;
    }

    /**
     * Get refEducateur
     *
     * @return string 
     */
    public function getRefEducateur()
    {
        return $this->refEducateur;
    }

    /**
     * Set refPhraseComposee
     *
     * @param string $refPhraseComposee
     * @return Enfant
     */
    public function setRefPhraseComposee($refPhraseComposee)
    {
        $this->refPhraseComposee = $refPhraseComposee;

        return $this;
    }

    /**
     * Get refPhraseComposee
     *
     * @return string 
     */
    public function getRefPhraseComposee()
    {
        return $this->refPhraseComposee;
    }

    /**
     * Set refExercice
     *
     * @param string $refExercice
     * @return Enfant
     */
    public function setRefExercice($refExercice)
    {
        $this->refExercice = $refExercice;

        return $this;
    }

    /**
     * Get refExercice
     *
     * @return string 
     */
    public function getRefExercice()
    {
        return $this->refExercice;
    }

    /**
     * Add refPhraseComposee
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\PhraseComposee $refPhraseComposee
     * @return Enfant
     */
    public function addRefPhraseComposee(\TerrePlurielle\Bundle\FoBundle\Entity\PhraseComposee $refPhraseComposee)
    {
        $this->refPhraseComposee[] = $refPhraseComposee;

        return $this;
    }

    /**
     * Remove refPhraseComposee
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\PhraseComposee $refPhraseComposee
     */
    public function removeRefPhraseComposee(\TerrePlurielle\Bundle\FoBundle\Entity\PhraseComposee $refPhraseComposee)
    {
        $this->refPhraseComposee->removeElement($refPhraseComposee);
    }

    /**
     * Add refExercice
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\Exercices $refExercice
     * @return Enfant
     */
    public function addRefExercice(\TerrePlurielle\Bundle\FoBundle\Entity\Exercices $refExercice)
    {
        $this->refExercice[] = $refExercice;

        return $this;
    }

    /**
     * Remove refExercice
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\Exercices $refExercice
     */
    public function removeRefExercice(\TerrePlurielle\Bundle\FoBundle\Entity\Exercices $refExercice)
    {
        $this->refExercice->removeElement($refExercice);
    }
}
