<?php

namespace TerrePlurielle\Bundle\FoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PhraseComposee
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TerrePlurielle\Bundle\FoBundle\Entity\PhraseComposeeRepository")
 */
class PhraseComposee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Contenu", type="string", length=255)
     */
    private $contenu;

    /**
     * @var string
     *
     * @ORM\Column(name="RefEnfant", type="string", length=255)
     */
    private $refEnfant;

    /**
     * @var string
     *
     * @ORM\Column(name="RefNiveau", type="string", length=255)
     */
    private $refNiveau;


    public function __toString()
    {
        return $this->contenu;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     * @return PhraseComposee
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string 
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set refEnfant
     *
     * @param string $refEnfant
     * @return PhraseComposee
     */
    public function setRefEnfant($refEnfant)
    {
        $this->refEnfant = $refEnfant;

        return $this;
    }

    /**
     * Get refEnfant
     *
     * @return string 
     */
    public function getRefEnfant()
    {
        return $this->refEnfant;
    }

    /**
     * Set refNiveau
     *
     * @param string $refNiveau
     * @return PhraseComposee
     */
    public function setRefNiveau($refNiveau)
    {
        $this->refNiveau = $refNiveau;

        return $this;
    }

    /**
     * Get refNiveau
     *
     * @return string 
     */
    public function getRefNiveau()
    {
        return $this->refNiveau;
    }
}
