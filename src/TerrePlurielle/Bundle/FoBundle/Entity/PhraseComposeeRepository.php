<?php

namespace TerrePlurielle\Bundle\FoBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * PhraseComposeeRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PhraseComposeeRepository extends EntityRepository
{
}
