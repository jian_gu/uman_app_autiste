<?php

// namespace TerrePlurielle\Bundle\FoBundle\Entity;

// use Doctrine\ORM\Mapping as ORM;

// /**
//  * Enfant_Exercices
//  *
//  * @ORM\Table()
//  * @ORM\Entity(repositoryClass="TerrePlurielle\Bundle\FoBundle\Entity\Enfant_ExercicesRepository")
//  */
// class Enfant_Exercices
// {
//     /**
//      * @var integer
//      *
//      * @ORM\Column(name="id", type="integer")
//      * @ORM\Id
//      * @ORM\GeneratedValue(strategy="AUTO")
//      */
//     private $id;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="refEnfant", type="string", length=255)
//      */
//     private $refEnfant;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="refExercice", type="string", length=255)
//      */
//     private $refExercice;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="Evaluation", type="string", length=255)
//      */
//     private $evaluation;


//     /**
//      * Get id
//      *
//      * @return integer 
//      */
//     public function getId()
//     {
//         return $this->id;
//     }

//     *
//      * Set refEnfant
//      *
//      * @param string $refEnfant
//      * @return Enfant_Exercices
     
//     public function setRefEnfant($refEnfant)
//     {
//         $this->refEnfant = $refEnfant;

//         return $this;
//     }

//     /**
//      * Get refEnfant
//      *
//      * @return string 
//      */
//     public function getRefEnfant()
//     {
//         return $this->refEnfant;
//     }

//     /**
//      * Set refExercice
//      *
//      * @param string $refExercice
//      * @return Enfant_Exercices
//      */
//     public function setRefExercice($refExercice)
//     {
//         $this->refExercice = $refExercice;

//         return $this;
//     }

//     /**
//      * Get refExercice
//      *
//      * @return string 
//      */
//     public function getRefExercice()
//     {
//         return $this->refExercice;
//     }

//     /**
//      * Set evaluation
//      *
//      * @param string $evaluation
//      * @return Enfant_Exercices
//      */
//     public function setEvaluation($evaluation)
//     {
//         $this->evaluation = $evaluation;

//         return $this;
//     }

//     /**
//      * Get evaluation
//      *
//      * @return string 
//      */
//     public function getEvaluation()
//     {
//         return $this->evaluation;
//     }
// }
