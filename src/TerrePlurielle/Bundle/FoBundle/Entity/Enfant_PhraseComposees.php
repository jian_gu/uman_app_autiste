<?php

// namespace TerrePlurielle\Bundle\FoBundle\Entity;

// use Doctrine\ORM\Mapping as ORM;

// /**
//  * Enfant_PhraseComposees
//  *
//  * @ORM\Table()
//  * @ORM\Entity(repositoryClass="TerrePlurielle\Bundle\FoBundle\Entity\Enfant_PhraseComposeesRepository")
//  */
// class Enfant_PhraseComposees
// {
//     /**
//      * @var integer
//      *
//      * @ORM\Column(name="id", type="integer")
//      * @ORM\Id
//      * @ORM\GeneratedValue(strategy="AUTO")
//      */
//     private $id;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="refEnfant", type="string", length=255)
//      */
//     private $refEnfant;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="refPhraseComposee", type="string", length=255)
//      */
//     private $refPhraseComposee;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="Attribute", type="string", length=255)
//      */
//     private $attribute;


//     /**
//      * Get id
//      *
//      * @return integer 
//      */
//     public function getId()
//     {
//         return $this->id;
//     }

//     /**
//      * Set refEnfant
//      *
//      * @param string $refEnfant
//      * @return Enfant_PhraseComposees
//      */
//     public function setRefEnfant($refEnfant)
//     {
//         $this->refEnfant = $refEnfant;

//         return $this;
//     }

//     /**
//      * Get refEnfant
//      *
//      * @return string 
//      */
//     public function getRefEnfant()
//     {
//         return $this->refEnfant;
//     }

//     /**
//      * Set refPhraseComposee
//      *
//      * @param string $refPhraseComposee
//      * @return Enfant_PhraseComposees
//      */
//     public function setRefPhraseComposee($refPhraseComposee)
//     {
//         $this->refPhraseComposee = $refPhraseComposee;

//         return $this;
//     }

//     /**
//      * Get refPhraseComposee
//      *
//      * @return string 
//      */
//     public function getRefPhraseComposee()
//     {
//         return $this->refPhraseComposee;
//     }

//     /**
//      * Set attribute
//      *
//      * @param string $attribute
//      * @return Enfant_PhraseComposees
//      */
//     public function setAttribute($attribute)
//     {
//         $this->attribute = $attribute;

//         return $this;
//     }

//     /**
//      * Get attribute
//      *
//      * @return string 
//      */
//     public function getAttribute()
//     {
//         return $this->attribute;
//     }
// }
