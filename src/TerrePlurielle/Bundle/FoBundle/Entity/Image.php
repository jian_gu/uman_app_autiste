<?php

namespace TerrePlurielle\Bundle\FoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Image
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TerrePlurielle\Bundle\FoBundle\Entity\ImageRepository")
 */
class Image
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Url", type="string", length=255)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="RefEnfant", type="string", length=255)
     */
    private $refEnfant;

    /**
     * @var string
     *
     * @ORM\Column(name="RefPictogramme", type="string", length=255)
     */
    private $refPictogramme;


    public function __toString()
    {
        return $this->url;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Image
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set refEnfant
     *
     * @param string $refEnfant
     * @return Image
     */
    public function setRefEnfant($refEnfant)
    {
        $this->refEnfant = $refEnfant;

        return $this;
    }

    /**
     * Get refEnfant
     *
     * @return string 
     */
    public function getRefEnfant()
    {
        return $this->refEnfant;
    }

    /**
     * Set refPictogramme
     *
     * @param string $refPictogramme
     * @return Image
     */
    public function setRefPictogramme($refPictogramme)
    {
        $this->refPictogramme = $refPictogramme;

        return $this;
    }

    /**
     * Get refPictogramme
     *
     * @return string 
     */
    public function getRefPictogramme()
    {
        return $this->refPictogramme;
    }
}
