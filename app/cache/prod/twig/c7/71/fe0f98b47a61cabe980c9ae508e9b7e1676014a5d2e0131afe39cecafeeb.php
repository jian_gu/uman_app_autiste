<?php

/* SonataCoreBundle:FlashMessage:render.html.twig */
class __TwigTemplate_c771fe0f98b47a61cabe980c9ae508e9b7e1676014a5d2e0131afe39cecafeeb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 11
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->env->getExtension('sonata_core_flashmessage')->getFlashMessagesTypes());
        foreach ($context['_seq'] as $context["_key"] => $context["type"]) {
            // line 12
            echo "    ";
            if (isset($context["domain"])) { $_domain_ = $context["domain"]; } else { $_domain_ = null; }
            $context["domain"] = ((array_key_exists("domain", $context)) ? ($_domain_) : (null));
            // line 13
            echo "    ";
            if (isset($context["type"])) { $_type_ = $context["type"]; } else { $_type_ = null; }
            if (isset($context["domain"])) { $_domain_ = $context["domain"]; } else { $_domain_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->env->getExtension('sonata_core_flashmessage')->getFlashMessages($_type_, $_domain_));
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 14
                echo "        <div class=\"alert alert-";
                if (isset($context["type"])) { $_type_ = $context["type"]; } else { $_type_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('sonata_core_status')->statusClass($_type_), "html", null, true);
                echo "\">
            ";
                // line 15
                if (isset($context["message"])) { $_message_ = $context["message"]; } else { $_message_ = null; }
                echo $_message_;
                echo "
        </div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['type'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "SonataCoreBundle:FlashMessage:render.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 14,  27 => 13,  23 => 12,  19 => 11,  30 => 3,  20 => 1,  919 => 274,  916 => 273,  911 => 268,  907 => 266,  900 => 263,  897 => 262,  894 => 261,  888 => 259,  879 => 258,  876 => 257,  872 => 256,  869 => 255,  862 => 253,  859 => 252,  856 => 251,  849 => 249,  846 => 248,  843 => 247,  836 => 245,  833 => 244,  830 => 243,  823 => 241,  820 => 240,  817 => 239,  814 => 238,  810 => 221,  805 => 218,  798 => 216,  795 => 215,  792 => 214,  778 => 213,  771 => 211,  766 => 208,  759 => 206,  749 => 204,  746 => 203,  743 => 202,  739 => 201,  719 => 200,  716 => 199,  713 => 198,  709 => 196,  705 => 195,  702 => 194,  698 => 191,  695 => 190,  692 => 189,  688 => 280,  686 => 273,  681 => 270,  679 => 238,  674 => 237,  671 => 236,  664 => 233,  661 => 232,  658 => 231,  653 => 228,  646 => 225,  643 => 224,  640 => 223,  637 => 222,  635 => 194,  631 => 192,  628 => 189,  625 => 188,  620 => 179,  616 => 174,  606 => 170,  600 => 168,  596 => 167,  593 => 166,  587 => 163,  583 => 162,  580 => 161,  574 => 160,  569 => 157,  563 => 156,  553 => 154,  549 => 153,  544 => 152,  538 => 150,  535 => 149,  531 => 148,  528 => 147,  525 => 146,  518 => 145,  515 => 144,  511 => 143,  507 => 142,  499 => 141,  495 => 140,  492 => 139,  489 => 137,  482 => 136,  478 => 135,  470 => 134,  466 => 133,  460 => 132,  456 => 131,  453 => 130,  447 => 129,  441 => 175,  439 => 166,  435 => 164,  432 => 163,  429 => 130,  427 => 129,  423 => 127,  420 => 126,  415 => 124,  407 => 120,  399 => 119,  394 => 118,  391 => 117,  384 => 182,  380 => 180,  378 => 179,  375 => 178,  373 => 126,  370 => 125,  368 => 124,  365 => 123,  363 => 117,  355 => 115,  353 => 114,  342 => 105,  339 => 104,  328 => 101,  321 => 80,  314 => 79,  310 => 78,  307 => 77,  301 => 75,  298 => 74,  293 => 72,  288 => 70,  283 => 68,  279 => 67,  275 => 66,  269 => 63,  265 => 61,  258 => 60,  249 => 59,  245 => 57,  242 => 56,  235 => 52,  231 => 51,  227 => 49,  212 => 38,  209 => 37,  204 => 35,  199 => 33,  195 => 32,  190 => 30,  187 => 29,  184 => 28,  178 => 22,  171 => 281,  169 => 188,  165 => 186,  163 => 104,  160 => 103,  156 => 101,  152 => 99,  149 => 98,  146 => 97,  128 => 95,  124 => 93,  120 => 92,  101 => 91,  98 => 90,  95 => 89,  88 => 87,  80 => 84,  76 => 82,  74 => 56,  71 => 55,  69 => 28,  60 => 22,  55 => 20,  53 => 19,  51 => 18,  49 => 17,  45 => 15,  43 => 14,  41 => 13,  39 => 12,  93 => 37,  85 => 86,  72 => 22,  63 => 17,  57 => 21,  50 => 9,  47 => 8,  40 => 15,  37 => 5,  32 => 5,  29 => 4,  26 => 2,);
    }
}
