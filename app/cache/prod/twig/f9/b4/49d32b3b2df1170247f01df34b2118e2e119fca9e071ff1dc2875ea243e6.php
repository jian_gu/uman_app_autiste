<?php

/* FoBundle:Communication:niveau2.html.twig */
class __TwigTemplate_f9b449d32b3b2df1170247f01df34b2118e2e119fca9e071ff1dc2875ea243e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Niveau2";
    }

    // line 4
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 5
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("/bundles/fo/css/jquery-ui-1.10.3.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("/bundles/fo/css/jquery-ui-1.10.4-smoothness.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("/bundles/fo/css/jquery.highlighttextarea.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <style>
        #gallery { 
            float: left; 
            width: 50%; 
            min-height: 6em; 
        }
        .theme-pictos { 
            float: left; 
            width: 50%; 
            min-height: 6em; 
        }
        .gallery li, .theme-gal li { 
            float: left; 
            //width: 96px; 
            padding: 0.4em; 
            margin: 0 0.4em 0.4em 0; 
            text-align: center; 
        }
        .theme-gal li:hover { 
            cursor: pointer;
        }
        #box { 
            float: left; 
            width: 32%; 
            min-height: 8em; 
            padding: 1%; 
        }
  
        .picto-div {
            width: 80px;
            height: 100px;
            text-align: center;
            border: solid;
            border-width: thin;
        }
        
        .picto-image {
            width: 80px;
            height: 80px;
            display: block;
        }
        .picto-mot, .picto-phrase {
            margin: auto;
        }
        .zone-text {
            display: inline-block;
        }
    
        .highlight { 
            background-color: yellow; 
        }
        
        // ======= niveau II ========
        .toggler { 
            width: 50%; 
            height: 200px; 
        }
        #button { 
            padding: .5em 1em; 
            text-decoration: none; 
        }
        #effect { 
            width: 80%; 
            height: 180px; 
            padding: 0.4em; 
            position: relative; 
            margin: auto;
        }
        #effect h3 { 
            margin: 0; 
            padding: 0.4em; 
            text-align: center; 
        }        
    </style>
";
    }

    // line 85
    public function block_body($context, array $blocks = array())
    {
        // line 86
        echo "    <div class=\"container ui-widget ui-helper-clearfix\">
    <h1>Communication - Niveau 2</h1>
        ";
        // line 89
        echo "        <div class=\"row part-1\" id=\"pool-pictos\">
            
            
            ";
        // line 93
        echo "            <ul id=\"ul-theme\" class=\"theme-gal ui-helper-reset ui-helper-clearfix\">
                ";
        // line 94
        if (isset($context["themes"])) { $_themes_ = $context["themes"]; } else { $_themes_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_themes_);
        foreach ($context['_seq'] as $context["_key"] => $context["theme"]) {
            // line 95
            echo "                <li class=\"ui-widget-content ui-corner-tr\">
                    <div class=\"theme-picto-div\" itemid=\"";
            // line 96
            if (isset($context["theme"])) { $_theme_ = $context["theme"]; } else { $_theme_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_theme_, "id"), "html", null, true);
            echo "\">
                        <img class=\"picto-image\" src=\"";
            // line 97
            if (isset($context["theme"])) { $_theme_ = $context["theme"]; } else { $_theme_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(("/bundles/fo/image/theme/" . $this->getAttribute($_theme_, "pictoUrl"))), "html", null, true);
            echo "\" alt=\"";
            if (isset($context["theme"])) { $_theme_ = $context["theme"]; } else { $_theme_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_theme_, "titre"), "html", null, true);
            echo "\">
                        <p class=\"picto-p\">";
            // line 98
            if (isset($context["theme"])) { $_theme_ = $context["theme"]; } else { $_theme_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_theme_, "titre"), "html", null, true);
            echo "</p>
                    </div>  
                </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['theme'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 102
        echo "            </ul>
            
            ";
        // line 105
        echo "            <div class=\"container\" id=\"themes-pictos-pool\">                
                ";
        // line 107
        echo "                ";
        if (isset($context["themes"])) { $_themes_ = $context["themes"]; } else { $_themes_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_themes_);
        foreach ($context['_seq'] as $context["_key"] => $context["theme"]) {
            // line 108
            echo "                <div id=\"div-theme-toggle-";
            if (isset($context["theme"])) { $_theme_ = $context["theme"]; } else { $_theme_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_theme_, "id"), "html", null, true);
            echo "\" class=\"container toggle theme-pictos\">
                    <ul id=\"gallery\" class=\"gallery ui-helper-reset ui-helper-clearfix ui-state-default\">
                    ";
            // line 111
            echo "                    ";
            if (isset($context["pictos"])) { $_pictos_ = $context["pictos"]; } else { $_pictos_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($_pictos_);
            foreach ($context['_seq'] as $context["_key"] => $context["picto"]) {
                // line 112
                echo "                        ";
                // line 113
                echo "                        ";
                if (isset($context["picto"])) { $_picto_ = $context["picto"]; } else { $_picto_ = null; }
                if ($this->getAttribute($this->getAttribute($_picto_, "refMot"), "refTheme")) {
                    // line 114
                    echo "                            ";
                    // line 115
                    echo "                            ";
                    if (isset($context["picto"])) { $_picto_ = $context["picto"]; } else { $_picto_ = null; }
                    if (isset($context["theme"])) { $_theme_ = $context["theme"]; } else { $_theme_ = null; }
                    if (($this->getAttribute($this->getAttribute($this->getAttribute($_picto_, "refMot"), "refTheme"), "id") == $this->getAttribute($_theme_, "id"))) {
                        // line 116
                        echo "                        <li class=\"ui-widget-content ui-corner-tr\">
                        <div id=\"div-pictos-toggle";
                        // line 117
                        if (isset($context["picto"])) { $_picto_ = $context["picto"]; } else { $_picto_ = null; }
                        echo twig_escape_filter($this->env, $this->getAttribute($_picto_, "id"), "html", null, true);
                        echo "\" class=\"picto-unit\" themeid=\"";
                        if (isset($context["picto"])) { $_picto_ = $context["picto"]; } else { $_picto_ = null; }
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_picto_, "refMot"), "refTheme"), "id"), "html", null, true);
                        echo "\">
                            <div class=\"picto-div\">
                                <img class=\"picto-image\" src=\"";
                        // line 119
                        if (isset($context["picto"])) { $_picto_ = $context["picto"]; } else { $_picto_ = null; }
                        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(("/bundles/fo/image/picto/" . $this->getAttribute($_picto_, "pictoUrl"))), "html", null, true);
                        echo "\" alt=\"";
                        if (isset($context["picto"])) { $_picto_ = $context["picto"]; } else { $_picto_ = null; }
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_picto_, "refMot"), "titre"), "html", null, true);
                        echo "\">
                                <p class=\"picto-mot\">";
                        // line 120
                        if (isset($context["picto"])) { $_picto_ = $context["picto"]; } else { $_picto_ = null; }
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_picto_, "refMot"), "titre"), "html", null, true);
                        echo "</p>
                                <p class=\"picto-phrase hidden\">";
                        // line 121
                        if (isset($context["picto"])) { $_picto_ = $context["picto"]; } else { $_picto_ = null; }
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_picto_, "refPhrase"), "contenu"), "html", null, true);
                        echo "</p>
                            </div> 
                        </div>
                        </li>
                            ";
                    }
                    // line 126
                    echo "                        ";
                }
                // line 127
                echo "                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['picto'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 128
            echo "                    </ul>
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['theme'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 131
        echo "            </div>
            
        </div>
        <hr/>
        ";
        // line 136
        echo "        <div class=\"row part-2\">
            <div id=\"box\" class=\"ui-widget-content ui-state-default\">
                
            </div>
            <div id=\"zone-text\">
                <p></p>
            </div>
            <button class=\"btn\" id=\"btn-play\">Lecture</button>
        </div>
    </div>
";
    }

    // line 148
    public function block_javascripts($context, array $blocks = array())
    {
        // line 149
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 153
        echo "    <script type=\"text/javascript\">
        \$(function() {
            // there's the gallery and the box
            var \$gallery = \$( \"#gallery\" ),
                \$box = \$( \"#box\" );
            
            
            // let the gallery items be draggable
            \$( \".gallery li\" ).draggable({
                cancel: \"a.ui-icon\", // clicking an icon won't initiate dragging
                revert: \"invalid\", // when not dropped, the item will revert back to its initial position
                containment: \".content\",
                helper: \"clone\",
                cursor: \"move\",
                snap: true,
                snapMode: 'inner'
              });
            
            // let the trash be droppable, accepting the gallery items
            \$box.droppable({
                accept: \".theme-pictos > .gallery > li\",
                activeClass: \"ui-state-highlight\",
                snap: true,
                snapMode: 'inner',
                drop: function( event, ui ) {
                  dropPicto( ui.draggable );
                }
            }); 
            
            // let the gallery be droppable as well, accepting items from the trash
            \$('.gallery').droppable({
              accept: \"#box ul li\",
              activeClass: \"ui-state-highlight\",
              drop: function( event, ui ) {
                restorerPicto( ui.draggable );
              }
            });
            
            /*
             * fonction mettre les pictos dans la box / la 2ere partie
             * @returns {undefined}
             */ 
            function dropPicto( \$item ) {
                \$item.fadeOut(function() {                    
                  var \$list = \$( \"ul\", \$box ).length ?
                    \$( \"ul\", \$box ) :
                    \$( \"<ul class='gallery ui-helper-reset'/>\" ).appendTo( \$box );                    
                    // Animation pour append
                    \$item.appendTo( \$list ).fadeIn(function() {
                        \$item
                          .animate({ width: \"auto\" })
                          .find( \"img\" )
                            .animate({ height: \"80px\" });
                    // desactiver le drag des pictos, 
                    //\$( \"li\", \$gallery ).draggable('disable');
                    // Recuperer tous les mots des items dans la box
                    var \$textArray = '';
                     
                        \$textArray = \$('#box > ul > li > .picto-unit > .picto-div > .picto-phrase').text();                        
                    console.log(\$textArray);
                    \$textArray = \$.trim(\$textArray);
                    
                    // Afficher le texte des pictos
                    \$('#zone-text')
                      .find('p')
                        .html(\$textArray);
                  });
                });
              }
              
            /*
             * Fonction restorer / re-mettre les pictos dans la 1ere partie
             * @returns {undefined}
             */
            function restorerPicto( \$item ) {
                var themeId = \$item.find('div').attr('themeId');
                console.log(themeId);
                \$item.fadeOut(function() {
                    \$item
                        .appendTo( \$('#div-theme-toggle-' + themeId + ' ul') )
                        .fadeIn();
                
                // Recuperer tous les mots des items dans la box
                var \$textArray = '';
                \$('#box > ul > li > .picto-unit > .picto-div > .picto-phrase').each(function () { 
                    \$textArray += ' ' + \$(this).text();                        
                });

                // Afficher le texte du picto
                \$('#zone-text')
                  .find('p')
                    .html(\$textArray);
                
                // activer le drag des pictos, 
                //\$( \"li\", \$gallery ).draggable('enable');
                
                });
            }
            
            // ============= Reading & highlight ===================
            \$('#btn-play').click(function () {
                audioPlay();
            });
            
            /*
            * Fonction de lire
            * @param {type} word
            * @returns {undefined}
            */
           function audioPlay() {
               // Diviser la phrase dans le textarea en mots
               var textArray = \$('#zone-text p').text().split(\" \");
               // Sil na pas de texte disponible, lancer une alerte et sarreter
               if (!\$.trim(textArray).length) {
                   alert ('Veuillez choisir un pictogramme');
                   return;
               }
               //console.log(textArray);
               // Initier lelement audio (pas de balise \"<audio>\")
               //var audio = document.createElement('audio');
               var audio = new Audio();
               // Initier lindex mot (0 pour le primer)
               var index = 0;
               // Initier la duree de temps
               var timeSpan = 0;

               /*
                * Boucle de traitement des mots: LIRE, mettre en SURBRILLANCE
                * @param {type} word
                * @returns {undefined}
                */
               function myLoop() {
                   // Boucler les action avec une pause indeterminee
                   setTimeout(function() {
                       // Effacer tout surbrillance
                       removeHighLight();
                       // Return when reaches the end of array
                       if (index === textArray.length) {
                           console.log('-- reading finished --');
                           return;
                       }

                       // Recuperer un element(mot) de lArray
                       var sWord = textArray[index];
                       // Recuperer le longueur du mot (pour definir le temps)
                       var sWordLength = sWord.length;

                       // Ici on va definir le temps de pronociation(time span) de mot
                       // Si le mot actuel est un non-chiffre
                       if (isNaN(sWord)) {
                           // ~12 characters will be read in 1 second
                           timeSpan = 500 * sWordLength / 6 + 1000;
                       } else {
                           //pour les chiffre, on definit le temps: 1.8s
                           timeSpan = 1800;
                       }
                       
                       // Utiliser la methode de notre site, epecher denvoyer le referer header.
                       audio.src = '";
        // line 311
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_app_, "request"), "getBaseURL", array(), "method"), "html", null, true);
        echo "/soundrequest?text_to_read=' + sWord;
                       audio.type = 'audio/mpeg';
                       // empecher denvoyer referer header
                       audio.rel = 'noreferrer';
                       audio.load();
                       audio.play();
                       
                       // Mettre le mot actuel en surbrillance
                       hightLightWord(sWord);

                       // incrementer lindex
                       index++;
                       myLoop();
                   }, timeSpan);

               }
               myLoop();
           }

           /*
            * Mettre un mot en surbrillance
            * @param {type} word
            * @returns {undefined}
            */
           function hightLightWord(word) {
               // active the highlight
               \$('#zone-text p').highlight(word,false);
           }

           /*
            * Effacer la surbrillance
            */
           function removeHighLight() {
               \$('#zone-text p').removeHighlight();
           }
           
           
           // ============ part 2 ==============
           // vvvvvvvvvvvv ====== vvvvvvvvvvvvvv
           // Cacher le div pictos par default
           \$('.theme-pictos').hide();
           var \$visible = false;

            // Afficher les pictos quand cliquer sur un theme
            \$( \"#ul-theme > li\" ).click(function() {
              //\$('#modal-pictos').modal('show');
              var currentThemeId = \$(this).find('div').attr('itemid');
              console.log(currentThemeId);
              \$('.theme-pictos').hide();
              \$('#div-theme-toggle-'+ currentThemeId).toggle('fast');
                            
              return false;
            });
            
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "FoBundle:Communication:niveau2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  459 => 311,  299 => 153,  294 => 149,  291 => 148,  277 => 136,  271 => 131,  263 => 128,  257 => 127,  254 => 126,  245 => 121,  240 => 120,  232 => 119,  223 => 117,  220 => 116,  215 => 115,  213 => 114,  209 => 113,  207 => 112,  201 => 111,  194 => 108,  188 => 107,  185 => 105,  181 => 102,  170 => 98,  162 => 97,  157 => 96,  154 => 95,  149 => 94,  146 => 93,  141 => 89,  137 => 86,  134 => 85,  54 => 8,  50 => 7,  46 => 6,  41 => 5,  38 => 4,  31 => 3,);
    }
}
