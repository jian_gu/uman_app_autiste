<?php

/* ::base.html.twig */
class __TwigTemplate_857c702b1c17da9ce43aa1a05eb2c72da7b589e9ed59a39164dd20291e288c46 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 11
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        <style type=\"text/css\">
            body{
                margin-top: 50px;
            }
        </style>
    </head>
    <body>
        <header class=\"navbar navbar-default navbar-fixed-top bs-docs-nav\" role=\"banner\">
            <div class=\"container\">
                ";
        // line 21
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 22
            echo "                <div class=\"navbar-header\">
                    <a href=\"";
            // line 23
            echo $this->env->getExtension('routing')->getPath("fos_user_profile");
            echo "\" class=\"navbar-brand\">";
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_app_, "user"), "username"), "html", null, true);
            echo "</a>
                </div>
                ";
        }
        // line 26
        echo "                <nav class=\"navbar-collapse bs-navbar-collapse\" role=\"navigation\">
                    <ul class=\"nav navbar-nav\">
                        <li class=\"\"><a href=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("fo_homepage");
        echo "\">Accueil</a></li>
                        <li class=\"\"><a href=\"";
        // line 29
        echo $this->env->getExtension('routing')->getPath("communication_nv1");
        echo "\">Niveau 1</a></li>
                        <li class=\"\"><a href=\"";
        // line 30
        echo $this->env->getExtension('routing')->getPath("communication_nv2");
        echo "\">Niveau 2</a></li>
                    </ul>

                    <ul class=\"nav navbar-nav navbar-right\">
                        <li><a href=\"";
        // line 34
        echo $this->env->getExtension('routing')->getPath("sonata_admin_dashboard");
        echo "\">Administration</a></li>
                    </ul>
                </nav>
            </div>
        </header>
        <div class=\"jumbotron\">
      <div class=\"container\">
        <h1>Hello, world!</h1>
        <p>Bienvenue sur l'applications numérique pour les enfants autistes</p>
        ";
        // line 44
        echo "      </div>
    </div>
        <div class=\"container\">
            ";
        // line 47
        $this->displayBlock('body', $context, $blocks);
        // line 48
        echo "        </div>
        ";
        // line 49
        $this->displayBlock('javascripts', $context, $blocks);
        // line 55
        echo "    </body>
</html>




    ";
        // line 62
        echo "    ";
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 63
            echo "        ";
            // line 64
            echo "        ";
            // line 65
            echo "
        ";
            // line 67
            echo "        ";
            if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
                // line 68
                echo "            ";
                // line 69
                echo "            ";
                // line 70
                echo "        ";
            }
            // line 71
            echo "    ";
        } else {
            // line 72
            echo "        ";
            // line 73
            echo "        ";
            // line 74
            echo "        ";
            // line 75
            echo "    ";
        }
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome!";
    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        echo "            
            <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("/bundles/fo/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
            <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("/bundles/fo/css/bootstrap.min2.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
            <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("/bundles/fo/css/bootstrap-responsive.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        ";
    }

    // line 47
    public function block_body($context, array $blocks = array())
    {
    }

    // line 49
    public function block_javascripts($context, array $blocks = array())
    {
        // line 50
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("/bundles/fo/js/jquery-1.9.1.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("/bundles/fo/js/jquery-ui-1.10.3.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("/bundles/fo/js/jquery-highlight1.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("/bundles/fo/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        ";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 53,  190 => 52,  186 => 51,  181 => 50,  178 => 49,  173 => 47,  167 => 9,  163 => 8,  159 => 7,  154 => 6,  148 => 5,  143 => 75,  141 => 74,  139 => 73,  137 => 72,  134 => 71,  131 => 70,  129 => 69,  127 => 68,  124 => 67,  119 => 64,  117 => 63,  114 => 62,  106 => 55,  104 => 49,  101 => 48,  99 => 47,  94 => 44,  75 => 30,  71 => 29,  67 => 28,  63 => 26,  54 => 23,  51 => 22,  49 => 21,  35 => 11,  33 => 6,  29 => 5,  23 => 1,  335 => 258,  177 => 102,  172 => 98,  169 => 97,  155 => 85,  149 => 80,  138 => 76,  130 => 75,  126 => 73,  121 => 65,  116 => 69,  112 => 66,  109 => 65,  82 => 34,  50 => 7,  46 => 6,  41 => 5,  38 => 4,  31 => 3,);
    }
}
