<?php

/* FoBundle:Communication:niveau1.html.twig */
class __TwigTemplate_d7a224da9654798c0d1bf5f10caf3f546432dc9575f402a78b0f143e4d3953d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Niveau1";
    }

    // line 4
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 5
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("/bundles/fo/css/jquery-ui-1.10.3.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("/bundles/fo/css/jquery.highlighttextarea.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
    <style>
        #gallery { 
            float: left; 
            width: 50%; 
            min-height: 12em; 
        }
        .gallery.custom-state-active { 
            background: #eee; 
        }
        .gallery li { 
            float: left; 
            //width: 96px; 
            padding: 0.4em; 
            margin: 0 0.4em 0.4em 0; 
            text-align: center; 
        }
        .gallery li:hover { 
            cursor: move;
        }
        #box { 
            float: left; 
            width: 32%; 
            min-height: 8em; 
            padding: 1%; 
        }
  
        
        
        ";
        // line 40
        echo "        .picto-div {
            width: 80px;
            height: 100px;
            text-align: center;
            border: solid;
            border-width: thin;
        }
        .picto-image {
            width: 80px;
            height: 80px;
            display: block;
        }
        .picto-p {
            margin: auto;
        }
        .zone-text {
            display: inline-block;
        }
    
        .highlight { 
            background-color: yellow; 
        }
    </style>
";
    }

    // line 65
    public function block_body($context, array $blocks = array())
    {
        // line 66
        echo "    <h1>Communication - Niveau 1</h1>
    <div class=\"content ui-widget ui-helper-clearfix\">
        ";
        // line 69
        echo "        <div class=\"container part-1\" id=\"pool-pictos\">
            
            <ul id=\"gallery\" class=\"gallery ui-helper-reset ui-helper-clearfix\">
                ";
        // line 72
        if (isset($context["pictos"])) { $_pictos_ = $context["pictos"]; } else { $_pictos_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_pictos_);
        foreach ($context['_seq'] as $context["_key"] => $context["picto"]) {
            // line 73
            echo "                <li class=\"ui-widget-content ui-corner-tr\">
                    <div class=\"picto-div\">
                        <img class=\"picto-image\" src=\"";
            // line 75
            if (isset($context["picto"])) { $_picto_ = $context["picto"]; } else { $_picto_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(("/bundles/fo/image/picto/" . $this->getAttribute($_picto_, "pictoUrl"))), "html", null, true);
            echo "\" alt=\"";
            if (isset($context["picto"])) { $_picto_ = $context["picto"]; } else { $_picto_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_picto_, "refMot"), "titre"), "html", null, true);
            echo "\">
                        <p class=\"picto-p\">";
            // line 76
            if (isset($context["picto"])) { $_picto_ = $context["picto"]; } else { $_picto_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_picto_, "refMot"), "titre"), "html", null, true);
            echo "</p>
                    </div>              
                </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['picto'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 80
        echo "            </ul>
            
        </div>
        <hr/>
        ";
        // line 85
        echo "        <div class=\"row part-2\">
            <div id=\"box\" class=\"ui-widget-content ui-state-default\">
                
            </div>
            <div id=\"zone-text\">
                <p></p>
            </div>
            <button class=\"btn\" id=\"btn-play\">Lecture</button>
        </div>
    </div>
";
    }

    // line 97
    public function block_javascripts($context, array $blocks = array())
    {
        // line 98
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 102
        echo "    <script type=\"text/javascript\">
        \$(function() {
            // there's the gallery and the box
            var \$gallery = \$( \"#gallery\" ),
                \$box = \$( \"#box\" );
            
            
            // let the gallery items be draggable
            \$( \"li\", \$gallery ).draggable({
                cancel: \"a.ui-icon\", // clicking an icon won't initiate dragging
                revert: \"invalid\", // when not dropped, the item will revert back to its initial position
                containment: \".content\",
                helper: \"clone\",
                cursor: \"move\",
                snap: true,
                snapMode: 'inner'
              });
            
            // let the trash be droppable, accepting the gallery items
            \$box.droppable({
                accept: \"#gallery > li\",
                activeClass: \"ui-state-highlight\",
                snap: true,
                snapMode: 'inner',
                drop: function( event, ui ) {
                  dropPicto( ui.draggable );
                }
            }); 
            
            // let the gallery be droppable as well, accepting items from the trash
            \$gallery.droppable({
              accept: \"#box li\",
              activeClass: \"ui-state-highlight\",
              drop: function( event, ui ) {
                restorerPicto( ui.draggable );
              }
            });
            
            /*
             * fonction mettre les pictos dans la box / la 2ere partie
             * @returns {undefined}
             */ 
            function dropPicto( \$item ) {
                \$item.fadeOut(function() {                    
                  var \$list = \$( \"ul\", \$box ).length ?
                    \$( \"ul\", \$box ) :
                    \$( \"<ul class='gallery ui-helper-reset'/>\" ).appendTo( \$box );                    
                    // Animation pour append
                    \$item.appendTo( \$list ).fadeIn(function() {
                        \$item
                          .animate({ width: \"auto\" })
                          .find( \"img\" )
                            .animate({ height: \"80px\" });
                    // desactiver le drag des pictos, 
                    //\$( \"li\", \$gallery ).draggable('disable');
                    // Recuperer tous les mots des items dans la box
                    var \$textArray = '';
                    \$('#box > ul > li > div > p').each(function () { 
                        \$textArray += \$(this).text() + ' ';                        
                    });
                    \$textArray = \$.trim(\$textArray);
                    
                    // Afficher le texte des pictos
                    \$('#zone-text')
                      .find('p')
                        .html(\$textArray);
                  });
                });
              }
              
            /*
             * Fonction restorer / re-mettre les pictos dans la 1ere partie
             * @returns {undefined}
             */
            function restorerPicto( \$item ) {
                \$item.fadeOut(function() {
                    \$item
                        .appendTo( \$gallery )
                        .fadeIn();
                
                // Recuperer tous les mots des items dans la box
                var \$textArray = '';
                \$('#box > ul > li > div > p').each(function () { 
                    \$textArray += ' ' + \$(this).text();                        
                });

                // Afficher le texte du picto
                \$('#zone-text')
                  .find('p')
                    .html(\$textArray);
                
                // activer le drag des pictos, 
                //\$( \"li\", \$gallery ).draggable('enable');
                
                });
            }
            
            // ============= Reading & highlight ===================
            \$('#btn-play').click(function () {
                audioPlay();
            });
            
            /*
            * Fonction de lire
            * @param {type} word
            * @returns {undefined}
            */
           function audioPlay() {
               // Diviser la phrase dans le textarea en mots
               var textArray = \$('#zone-text p').text().split(\" \");
               // Sil na pas de texte disponible, lancer une alerte et sarreter
               if (!\$.trim(textArray).length) {
                   alert ('Veuillez choisir un pictogramme');
                   return;
               }
               //console.log(textArray);
               // Initier lelement audio (pas de balise \"<audio>\")
               //var audio = document.createElement('audio');
               var audio = new Audio();
               // Initier lindex mot (0 pour le primer)
               var index = 0;
               // Initier la duree de temps
               var timeSpan = 0;

               /*
                * Boucle de traitement des mots: LIRE, mettre en SURBRILLANCE
                * @param {type} word
                * @returns {undefined}
                */
               function myLoop() {
                   // Boucler les action avec une pause indeterminee
                   setTimeout(function() {
                       // Effacer tout surbrillance
                       removeHighLight();
                       // Return when reaches the end of array
                       if (index === textArray.length) {
                           console.log('-- reading finished --');
                           return;
                       }

                       // Recuperer un element(mot) de lArray
                       var sWord = textArray[index];
                       // Recuperer le longueur du mot (pour definir le temps)
                       var sWordLength = sWord.length;

                       // Ici on va definir le temps de pronociation(time span) de mot
                       // Si le mot actuel est un non-chiffre
                       if (isNaN(sWord)) {
                           // ~12 characters will be read in 1 second
                           timeSpan = 500 * sWordLength / 6 + 1000;
                       } else {
                           //pour les chiffre, on definit le temps: 1.8s
                           timeSpan = 1800;
                       }
                       
                       // Utiliser la methode de notre site, epecher denvoyer le referer header.
                       audio.src = '";
        // line 258
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_app_, "request"), "getBaseURL", array(), "method"), "html", null, true);
        echo "/soundrequest?text_to_read=' + sWord;
                       audio.type = 'audio/mpeg';
                       // empecher denvoyer referer header
                       audio.rel = 'noreferrer';
                       audio.load();
                       audio.play();
                       
                       // Mettre le mot actuel en surbrillance
                       hightLightWord(sWord);

                       // incrementer lindex
                       index++;
                       myLoop();
                   }, timeSpan);

               }
               myLoop();
           }

           /*
            * Mettre un mot en surbrillance
            * @param {type} word
            * @returns {undefined}
            */
           function hightLightWord(word) {
               // active the highlight
               \$('#zone-text p').highlight(word,false);
           }

           /*
            * Effacer la surbrillance
            */
           function removeHighLight() {
               \$('#zone-text p').removeHighlight();
           }
            
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "FoBundle:Communication:niveau1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  335 => 258,  177 => 102,  172 => 98,  169 => 97,  155 => 85,  149 => 80,  138 => 76,  130 => 75,  126 => 73,  121 => 72,  116 => 69,  112 => 66,  109 => 65,  82 => 40,  50 => 7,  46 => 6,  41 => 5,  38 => 4,  31 => 3,);
    }
}
