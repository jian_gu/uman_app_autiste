<?php

namespace Proxies\__CG__\TerrePlurielle\Bundle\FoBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Pictogramme extends \TerrePlurielle\Bundle\FoBundle\Entity\Pictogramme implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array();



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return array('__isInitialized__', 'id', 'pictoUrl', 'imgUrl', 'refMot', 'refPhrase', 'refTheme', 'pictoFile');
        }

        return array('__isInitialized__', 'id', 'pictoUrl', 'imgUrl', 'refMot', 'refPhrase', 'refTheme', 'pictoFile');
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Pictogramme $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', array());
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', array());
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function setPictoFile(\Symfony\Component\HttpFoundation\File\UploadedFile $pictoFile = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPictoFile', array($pictoFile));

        return parent::setPictoFile($pictoFile);
    }

    /**
     * {@inheritDoc}
     */
    public function getPictoFile()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPictoFile', array());

        return parent::getPictoFile();
    }

    /**
     * {@inheritDoc}
     */
    public function getAbsolutePathPicto()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAbsolutePathPicto', array());

        return parent::getAbsolutePathPicto();
    }

    /**
     * {@inheritDoc}
     */
    public function getWebPathPicto()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getWebPathPicto', array());

        return parent::getWebPathPicto();
    }

    /**
     * {@inheritDoc}
     */
    public function upload($basepath)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'upload', array($basepath));

        return parent::upload($basepath);
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', array());

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setPictoUrl($pictoUrl)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPictoUrl', array($pictoUrl));

        return parent::setPictoUrl($pictoUrl);
    }

    /**
     * {@inheritDoc}
     */
    public function getPictoUrl()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPictoUrl', array());

        return parent::getPictoUrl();
    }

    /**
     * {@inheritDoc}
     */
    public function setImgUrl($imgUrl)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setImgUrl', array($imgUrl));

        return parent::setImgUrl($imgUrl);
    }

    /**
     * {@inheritDoc}
     */
    public function getImgUrl()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getImgUrl', array());

        return parent::getImgUrl();
    }

    /**
     * {@inheritDoc}
     */
    public function setRefTheme($refTheme)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRefTheme', array($refTheme));

        return parent::setRefTheme($refTheme);
    }

    /**
     * {@inheritDoc}
     */
    public function getRefTheme()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRefTheme', array());

        return parent::getRefTheme();
    }

    /**
     * {@inheritDoc}
     */
    public function setRefMot(\TerrePlurielle\Bundle\FoBundle\Entity\Mot $refMot = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRefMot', array($refMot));

        return parent::setRefMot($refMot);
    }

    /**
     * {@inheritDoc}
     */
    public function getRefMot()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRefMot', array());

        return parent::getRefMot();
    }

    /**
     * {@inheritDoc}
     */
    public function setRefPhrase(\TerrePlurielle\Bundle\FoBundle\Entity\PhraseProposee $refPhrase = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRefPhrase', array($refPhrase));

        return parent::setRefPhrase($refPhrase);
    }

    /**
     * {@inheritDoc}
     */
    public function getRefPhrase()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRefPhrase', array());

        return parent::getRefPhrase();
    }

}
