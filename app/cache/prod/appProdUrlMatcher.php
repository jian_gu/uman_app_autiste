<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // bo_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'bo_homepage')), array (  '_controller' => 'TerrePlurielle\\Bundle\\BoBundle\\Controller\\DefaultController::indexAction',));
        }

        // fo_homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'fo_homepage');
            }

            return array (  '_controller' => 'TerrePlurielle\\Bundle\\FoBundle\\Controller\\HomeController::indexAction',  '_route' => 'fo_homepage',);
        }

        if (0 === strpos($pathinfo, '/niveau')) {
            // communication_nv1
            if ($pathinfo === '/niveau1') {
                return array (  '_controller' => 'TerrePlurielle\\Bundle\\FoBundle\\Controller\\CommunicationController::niveau1Action',  '_route' => 'communication_nv1',);
            }

            // communication_nv2
            if ($pathinfo === '/niveau2') {
                return array (  '_controller' => 'TerrePlurielle\\Bundle\\FoBundle\\Controller\\CommunicationController::niveau2Action',  '_route' => 'communication_nv2',);
            }

            // communication_nv3
            if ($pathinfo === '/niveau3') {
                return array (  '_controller' => 'TerrePlurielle\\Bundle\\FoBundle\\Controller\\CommunicationController::niveau3Action',  '_route' => 'communication_nv3',);
            }

        }

        // get_pictos_by_theme
        if ($pathinfo === '/pictos_by_theme') {
            return array (  '_controller' => 'TerrePlurielle\\Bundle\\FoBundle\\Controller\\CommunicationController::getPictosByThemeAction',  '_route' => 'get_pictos_by_theme',);
        }

        // sound_request
        if ($pathinfo === '/soundrequest') {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_sound_request;
            }

            return array (  '_controller' => 'TerrePlurielle\\Bundle\\FoBundle\\Controller\\CommunicationController::soundRequestAction',  '_route' => 'sound_request',);
        }
        not_sound_request:

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ($pathinfo === '/login') {
                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }

                // fos_user_security_check
                if ($pathinfo === '/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ($pathinfo === '/logout') {
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ($pathinfo === '/profile/edit') {
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }

        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/register')) {
                // fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/register') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                    }

                    return array (  '_controller' => 'TerrePlurielle\\Bundle\\BoBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
                }

                if (0 === strpos($pathinfo, '/register/c')) {
                    // fos_user_registration_check_email
                    if ($pathinfo === '/register/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'TerrePlurielle\\Bundle\\BoBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/register/confirm')) {
                        // fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'TerrePlurielle\\Bundle\\BoBundle\\Controller\\RegistrationController::confirmAction',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ($pathinfo === '/register/confirmed') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'TerrePlurielle\\Bundle\\BoBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/resetting')) {
                // fos_user_resetting_request
                if ($pathinfo === '/resetting/request') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_request;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
                }
                not_fos_user_resetting_request:

                // fos_user_resetting_send_email
                if ($pathinfo === '/resetting/send-email') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_resetting_send_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                }
                not_fos_user_resetting_send_email:

                // fos_user_resetting_check_email
                if ($pathinfo === '/resetting/check-email') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_check_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                }
                not_fos_user_resetting_check_email:

                // fos_user_resetting_reset
                if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_resetting_reset;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
                }
                not_fos_user_resetting_reset:

            }

        }

        // fos_user_change_password
        if ($pathinfo === '/change-password/change-password') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }

            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
        }
        not_fos_user_change_password:

        if (0 === strpos($pathinfo, '/admin')) {
            if (0 === strpos($pathinfo, '/admin/log')) {
                if (0 === strpos($pathinfo, '/admin/login')) {
                    // sonata_user_admin_security_login
                    if ($pathinfo === '/admin/login') {
                        return array (  '_controller' => 'Sonata\\UserBundle\\Controller\\AdminSecurityController::loginAction',  '_route' => 'sonata_user_admin_security_login',);
                    }

                    // sonata_user_admin_security_check
                    if ($pathinfo === '/admin/login_check') {
                        return array (  '_controller' => 'Sonata\\UserBundle\\Controller\\AdminSecurityController::checkAction',  '_route' => 'sonata_user_admin_security_check',);
                    }

                }

                // sonata_user_admin_security_logout
                if ($pathinfo === '/admin/logout') {
                    return array (  '_controller' => 'Sonata\\UserBundle\\Controller\\AdminSecurityController::logoutAction',  '_route' => 'sonata_user_admin_security_logout',);
                }

            }

            // sonata_admin_redirect
            if (rtrim($pathinfo, '/') === '/admin') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'sonata_admin_redirect');
                }

                return array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController::redirectAction',  'route' => 'sonata_admin_dashboard',  'permanent' => 'true',  '_route' => 'sonata_admin_redirect',);
            }

            // sonata_admin_dashboard
            if ($pathinfo === '/admin/dashboard') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::dashboardAction',  '_route' => 'sonata_admin_dashboard',);
            }

            if (0 === strpos($pathinfo, '/admin/core')) {
                // sonata_admin_retrieve_form_element
                if ($pathinfo === '/admin/core/get-form-field-element') {
                    return array (  '_controller' => 'sonata.admin.controller.admin:retrieveFormFieldElementAction',  '_route' => 'sonata_admin_retrieve_form_element',);
                }

                // sonata_admin_append_form_element
                if ($pathinfo === '/admin/core/append-form-field-element') {
                    return array (  '_controller' => 'sonata.admin.controller.admin:appendFormFieldElementAction',  '_route' => 'sonata_admin_append_form_element',);
                }

                // sonata_admin_short_object_information
                if (0 === strpos($pathinfo, '/admin/core/get-short-object-description') && preg_match('#^/admin/core/get\\-short\\-object\\-description(?:\\.(?P<_format>html|json))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_short_object_information')), array (  '_controller' => 'sonata.admin.controller.admin:getShortObjectDescriptionAction',  '_format' => 'html',));
                }

                // sonata_admin_set_object_field_value
                if ($pathinfo === '/admin/core/set-object-field-value') {
                    return array (  '_controller' => 'sonata.admin.controller.admin:setObjectFieldValueAction',  '_route' => 'sonata_admin_set_object_field_value',);
                }

            }

            // sonata_admin_search
            if ($pathinfo === '/admin/search') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::searchAction',  '_route' => 'sonata_admin_search',);
            }

            if (0 === strpos($pathinfo, '/admin/terreplurielle/fo')) {
                if (0 === strpos($pathinfo, '/admin/terreplurielle/fo/activite')) {
                    // admin_terreplurielle_fo_activite_list
                    if ($pathinfo === '/admin/terreplurielle/fo/activite/list') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.activite',  '_sonata_name' => 'admin_terreplurielle_fo_activite_list',  '_route' => 'admin_terreplurielle_fo_activite_list',);
                    }

                    // admin_terreplurielle_fo_activite_create
                    if ($pathinfo === '/admin/terreplurielle/fo/activite/create') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.activite',  '_sonata_name' => 'admin_terreplurielle_fo_activite_create',  '_route' => 'admin_terreplurielle_fo_activite_create',);
                    }

                    // admin_terreplurielle_fo_activite_batch
                    if ($pathinfo === '/admin/terreplurielle/fo/activite/batch') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.activite',  '_sonata_name' => 'admin_terreplurielle_fo_activite_batch',  '_route' => 'admin_terreplurielle_fo_activite_batch',);
                    }

                    // admin_terreplurielle_fo_activite_edit
                    if (preg_match('#^/admin/terreplurielle/fo/activite/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_activite_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.activite',  '_sonata_name' => 'admin_terreplurielle_fo_activite_edit',));
                    }

                    // admin_terreplurielle_fo_activite_delete
                    if (preg_match('#^/admin/terreplurielle/fo/activite/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_activite_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.activite',  '_sonata_name' => 'admin_terreplurielle_fo_activite_delete',));
                    }

                    // admin_terreplurielle_fo_activite_show
                    if (preg_match('#^/admin/terreplurielle/fo/activite/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_activite_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.activite',  '_sonata_name' => 'admin_terreplurielle_fo_activite_show',));
                    }

                    // admin_terreplurielle_fo_activite_export
                    if ($pathinfo === '/admin/terreplurielle/fo/activite/export') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.activite',  '_sonata_name' => 'admin_terreplurielle_fo_activite_export',  '_route' => 'admin_terreplurielle_fo_activite_export',);
                    }

                }

                if (0 === strpos($pathinfo, '/admin/terreplurielle/fo/e')) {
                    if (0 === strpos($pathinfo, '/admin/terreplurielle/fo/educateur')) {
                        // admin_terreplurielle_fo_educateur_list
                        if ($pathinfo === '/admin/terreplurielle/fo/educateur/list') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.educateur',  '_sonata_name' => 'admin_terreplurielle_fo_educateur_list',  '_route' => 'admin_terreplurielle_fo_educateur_list',);
                        }

                        // admin_terreplurielle_fo_educateur_create
                        if ($pathinfo === '/admin/terreplurielle/fo/educateur/create') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.educateur',  '_sonata_name' => 'admin_terreplurielle_fo_educateur_create',  '_route' => 'admin_terreplurielle_fo_educateur_create',);
                        }

                        // admin_terreplurielle_fo_educateur_batch
                        if ($pathinfo === '/admin/terreplurielle/fo/educateur/batch') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.educateur',  '_sonata_name' => 'admin_terreplurielle_fo_educateur_batch',  '_route' => 'admin_terreplurielle_fo_educateur_batch',);
                        }

                        // admin_terreplurielle_fo_educateur_edit
                        if (preg_match('#^/admin/terreplurielle/fo/educateur/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_educateur_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.educateur',  '_sonata_name' => 'admin_terreplurielle_fo_educateur_edit',));
                        }

                        // admin_terreplurielle_fo_educateur_delete
                        if (preg_match('#^/admin/terreplurielle/fo/educateur/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_educateur_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.educateur',  '_sonata_name' => 'admin_terreplurielle_fo_educateur_delete',));
                        }

                        // admin_terreplurielle_fo_educateur_show
                        if (preg_match('#^/admin/terreplurielle/fo/educateur/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_educateur_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.educateur',  '_sonata_name' => 'admin_terreplurielle_fo_educateur_show',));
                        }

                        // admin_terreplurielle_fo_educateur_export
                        if ($pathinfo === '/admin/terreplurielle/fo/educateur/export') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.educateur',  '_sonata_name' => 'admin_terreplurielle_fo_educateur_export',  '_route' => 'admin_terreplurielle_fo_educateur_export',);
                        }

                    }

                    if (0 === strpos($pathinfo, '/admin/terreplurielle/fo/enfant')) {
                        // admin_terreplurielle_fo_enfant_list
                        if ($pathinfo === '/admin/terreplurielle/fo/enfant/list') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.enfant',  '_sonata_name' => 'admin_terreplurielle_fo_enfant_list',  '_route' => 'admin_terreplurielle_fo_enfant_list',);
                        }

                        // admin_terreplurielle_fo_enfant_create
                        if ($pathinfo === '/admin/terreplurielle/fo/enfant/create') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.enfant',  '_sonata_name' => 'admin_terreplurielle_fo_enfant_create',  '_route' => 'admin_terreplurielle_fo_enfant_create',);
                        }

                        // admin_terreplurielle_fo_enfant_batch
                        if ($pathinfo === '/admin/terreplurielle/fo/enfant/batch') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.enfant',  '_sonata_name' => 'admin_terreplurielle_fo_enfant_batch',  '_route' => 'admin_terreplurielle_fo_enfant_batch',);
                        }

                        // admin_terreplurielle_fo_enfant_edit
                        if (preg_match('#^/admin/terreplurielle/fo/enfant/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_enfant_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.enfant',  '_sonata_name' => 'admin_terreplurielle_fo_enfant_edit',));
                        }

                        // admin_terreplurielle_fo_enfant_delete
                        if (preg_match('#^/admin/terreplurielle/fo/enfant/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_enfant_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.enfant',  '_sonata_name' => 'admin_terreplurielle_fo_enfant_delete',));
                        }

                        // admin_terreplurielle_fo_enfant_show
                        if (preg_match('#^/admin/terreplurielle/fo/enfant/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_enfant_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.enfant',  '_sonata_name' => 'admin_terreplurielle_fo_enfant_show',));
                        }

                        // admin_terreplurielle_fo_enfant_export
                        if ($pathinfo === '/admin/terreplurielle/fo/enfant/export') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.enfant',  '_sonata_name' => 'admin_terreplurielle_fo_enfant_export',  '_route' => 'admin_terreplurielle_fo_enfant_export',);
                        }

                    }

                    if (0 === strpos($pathinfo, '/admin/terreplurielle/fo/exercices')) {
                        // admin_terreplurielle_fo_exercices_list
                        if ($pathinfo === '/admin/terreplurielle/fo/exercices/list') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.exercices',  '_sonata_name' => 'admin_terreplurielle_fo_exercices_list',  '_route' => 'admin_terreplurielle_fo_exercices_list',);
                        }

                        // admin_terreplurielle_fo_exercices_create
                        if ($pathinfo === '/admin/terreplurielle/fo/exercices/create') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.exercices',  '_sonata_name' => 'admin_terreplurielle_fo_exercices_create',  '_route' => 'admin_terreplurielle_fo_exercices_create',);
                        }

                        // admin_terreplurielle_fo_exercices_batch
                        if ($pathinfo === '/admin/terreplurielle/fo/exercices/batch') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.exercices',  '_sonata_name' => 'admin_terreplurielle_fo_exercices_batch',  '_route' => 'admin_terreplurielle_fo_exercices_batch',);
                        }

                        // admin_terreplurielle_fo_exercices_edit
                        if (preg_match('#^/admin/terreplurielle/fo/exercices/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_exercices_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.exercices',  '_sonata_name' => 'admin_terreplurielle_fo_exercices_edit',));
                        }

                        // admin_terreplurielle_fo_exercices_delete
                        if (preg_match('#^/admin/terreplurielle/fo/exercices/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_exercices_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.exercices',  '_sonata_name' => 'admin_terreplurielle_fo_exercices_delete',));
                        }

                        // admin_terreplurielle_fo_exercices_show
                        if (preg_match('#^/admin/terreplurielle/fo/exercices/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_exercices_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.exercices',  '_sonata_name' => 'admin_terreplurielle_fo_exercices_show',));
                        }

                        // admin_terreplurielle_fo_exercices_export
                        if ($pathinfo === '/admin/terreplurielle/fo/exercices/export') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.exercices',  '_sonata_name' => 'admin_terreplurielle_fo_exercices_export',  '_route' => 'admin_terreplurielle_fo_exercices_export',);
                        }

                    }

                }

                if (0 === strpos($pathinfo, '/admin/terreplurielle/fo/image')) {
                    // admin_terreplurielle_fo_image_list
                    if ($pathinfo === '/admin/terreplurielle/fo/image/list') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.image',  '_sonata_name' => 'admin_terreplurielle_fo_image_list',  '_route' => 'admin_terreplurielle_fo_image_list',);
                    }

                    // admin_terreplurielle_fo_image_create
                    if ($pathinfo === '/admin/terreplurielle/fo/image/create') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.image',  '_sonata_name' => 'admin_terreplurielle_fo_image_create',  '_route' => 'admin_terreplurielle_fo_image_create',);
                    }

                    // admin_terreplurielle_fo_image_batch
                    if ($pathinfo === '/admin/terreplurielle/fo/image/batch') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.image',  '_sonata_name' => 'admin_terreplurielle_fo_image_batch',  '_route' => 'admin_terreplurielle_fo_image_batch',);
                    }

                    // admin_terreplurielle_fo_image_edit
                    if (preg_match('#^/admin/terreplurielle/fo/image/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_image_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.image',  '_sonata_name' => 'admin_terreplurielle_fo_image_edit',));
                    }

                    // admin_terreplurielle_fo_image_delete
                    if (preg_match('#^/admin/terreplurielle/fo/image/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_image_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.image',  '_sonata_name' => 'admin_terreplurielle_fo_image_delete',));
                    }

                    // admin_terreplurielle_fo_image_show
                    if (preg_match('#^/admin/terreplurielle/fo/image/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_image_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.image',  '_sonata_name' => 'admin_terreplurielle_fo_image_show',));
                    }

                    // admin_terreplurielle_fo_image_export
                    if ($pathinfo === '/admin/terreplurielle/fo/image/export') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.image',  '_sonata_name' => 'admin_terreplurielle_fo_image_export',  '_route' => 'admin_terreplurielle_fo_image_export',);
                    }

                }

                if (0 === strpos($pathinfo, '/admin/terreplurielle/fo/mot')) {
                    // admin_terreplurielle_fo_mot_list
                    if ($pathinfo === '/admin/terreplurielle/fo/mot/list') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.mot',  '_sonata_name' => 'admin_terreplurielle_fo_mot_list',  '_route' => 'admin_terreplurielle_fo_mot_list',);
                    }

                    // admin_terreplurielle_fo_mot_create
                    if ($pathinfo === '/admin/terreplurielle/fo/mot/create') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.mot',  '_sonata_name' => 'admin_terreplurielle_fo_mot_create',  '_route' => 'admin_terreplurielle_fo_mot_create',);
                    }

                    // admin_terreplurielle_fo_mot_batch
                    if ($pathinfo === '/admin/terreplurielle/fo/mot/batch') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.mot',  '_sonata_name' => 'admin_terreplurielle_fo_mot_batch',  '_route' => 'admin_terreplurielle_fo_mot_batch',);
                    }

                    // admin_terreplurielle_fo_mot_edit
                    if (preg_match('#^/admin/terreplurielle/fo/mot/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_mot_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.mot',  '_sonata_name' => 'admin_terreplurielle_fo_mot_edit',));
                    }

                    // admin_terreplurielle_fo_mot_delete
                    if (preg_match('#^/admin/terreplurielle/fo/mot/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_mot_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.mot',  '_sonata_name' => 'admin_terreplurielle_fo_mot_delete',));
                    }

                    // admin_terreplurielle_fo_mot_show
                    if (preg_match('#^/admin/terreplurielle/fo/mot/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_mot_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.mot',  '_sonata_name' => 'admin_terreplurielle_fo_mot_show',));
                    }

                    // admin_terreplurielle_fo_mot_export
                    if ($pathinfo === '/admin/terreplurielle/fo/mot/export') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.mot',  '_sonata_name' => 'admin_terreplurielle_fo_mot_export',  '_route' => 'admin_terreplurielle_fo_mot_export',);
                    }

                }

                if (0 === strpos($pathinfo, '/admin/terreplurielle/fo/niveau')) {
                    // admin_terreplurielle_fo_niveau_list
                    if ($pathinfo === '/admin/terreplurielle/fo/niveau/list') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.niveau',  '_sonata_name' => 'admin_terreplurielle_fo_niveau_list',  '_route' => 'admin_terreplurielle_fo_niveau_list',);
                    }

                    // admin_terreplurielle_fo_niveau_create
                    if ($pathinfo === '/admin/terreplurielle/fo/niveau/create') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.niveau',  '_sonata_name' => 'admin_terreplurielle_fo_niveau_create',  '_route' => 'admin_terreplurielle_fo_niveau_create',);
                    }

                    // admin_terreplurielle_fo_niveau_batch
                    if ($pathinfo === '/admin/terreplurielle/fo/niveau/batch') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.niveau',  '_sonata_name' => 'admin_terreplurielle_fo_niveau_batch',  '_route' => 'admin_terreplurielle_fo_niveau_batch',);
                    }

                    // admin_terreplurielle_fo_niveau_edit
                    if (preg_match('#^/admin/terreplurielle/fo/niveau/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_niveau_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.niveau',  '_sonata_name' => 'admin_terreplurielle_fo_niveau_edit',));
                    }

                    // admin_terreplurielle_fo_niveau_delete
                    if (preg_match('#^/admin/terreplurielle/fo/niveau/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_niveau_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.niveau',  '_sonata_name' => 'admin_terreplurielle_fo_niveau_delete',));
                    }

                    // admin_terreplurielle_fo_niveau_show
                    if (preg_match('#^/admin/terreplurielle/fo/niveau/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_niveau_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.niveau',  '_sonata_name' => 'admin_terreplurielle_fo_niveau_show',));
                    }

                    // admin_terreplurielle_fo_niveau_export
                    if ($pathinfo === '/admin/terreplurielle/fo/niveau/export') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.niveau',  '_sonata_name' => 'admin_terreplurielle_fo_niveau_export',  '_route' => 'admin_terreplurielle_fo_niveau_export',);
                    }

                }

                if (0 === strpos($pathinfo, '/admin/terreplurielle/fo/p')) {
                    if (0 === strpos($pathinfo, '/admin/terreplurielle/fo/parents')) {
                        // admin_terreplurielle_fo_parents_list
                        if ($pathinfo === '/admin/terreplurielle/fo/parents/list') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.parent',  '_sonata_name' => 'admin_terreplurielle_fo_parents_list',  '_route' => 'admin_terreplurielle_fo_parents_list',);
                        }

                        // admin_terreplurielle_fo_parents_create
                        if ($pathinfo === '/admin/terreplurielle/fo/parents/create') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.parent',  '_sonata_name' => 'admin_terreplurielle_fo_parents_create',  '_route' => 'admin_terreplurielle_fo_parents_create',);
                        }

                        // admin_terreplurielle_fo_parents_batch
                        if ($pathinfo === '/admin/terreplurielle/fo/parents/batch') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.parent',  '_sonata_name' => 'admin_terreplurielle_fo_parents_batch',  '_route' => 'admin_terreplurielle_fo_parents_batch',);
                        }

                        // admin_terreplurielle_fo_parents_edit
                        if (preg_match('#^/admin/terreplurielle/fo/parents/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_parents_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.parent',  '_sonata_name' => 'admin_terreplurielle_fo_parents_edit',));
                        }

                        // admin_terreplurielle_fo_parents_delete
                        if (preg_match('#^/admin/terreplurielle/fo/parents/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_parents_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.parent',  '_sonata_name' => 'admin_terreplurielle_fo_parents_delete',));
                        }

                        // admin_terreplurielle_fo_parents_show
                        if (preg_match('#^/admin/terreplurielle/fo/parents/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_parents_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.parent',  '_sonata_name' => 'admin_terreplurielle_fo_parents_show',));
                        }

                        // admin_terreplurielle_fo_parents_export
                        if ($pathinfo === '/admin/terreplurielle/fo/parents/export') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.parent',  '_sonata_name' => 'admin_terreplurielle_fo_parents_export',  '_route' => 'admin_terreplurielle_fo_parents_export',);
                        }

                    }

                    if (0 === strpos($pathinfo, '/admin/terreplurielle/fo/pictogramme')) {
                        // admin_terreplurielle_fo_pictogramme_list
                        if ($pathinfo === '/admin/terreplurielle/fo/pictogramme/list') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.pictogramme',  '_sonata_name' => 'admin_terreplurielle_fo_pictogramme_list',  '_route' => 'admin_terreplurielle_fo_pictogramme_list',);
                        }

                        // admin_terreplurielle_fo_pictogramme_create
                        if ($pathinfo === '/admin/terreplurielle/fo/pictogramme/create') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.pictogramme',  '_sonata_name' => 'admin_terreplurielle_fo_pictogramme_create',  '_route' => 'admin_terreplurielle_fo_pictogramme_create',);
                        }

                        // admin_terreplurielle_fo_pictogramme_batch
                        if ($pathinfo === '/admin/terreplurielle/fo/pictogramme/batch') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.pictogramme',  '_sonata_name' => 'admin_terreplurielle_fo_pictogramme_batch',  '_route' => 'admin_terreplurielle_fo_pictogramme_batch',);
                        }

                        // admin_terreplurielle_fo_pictogramme_edit
                        if (preg_match('#^/admin/terreplurielle/fo/pictogramme/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_pictogramme_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.pictogramme',  '_sonata_name' => 'admin_terreplurielle_fo_pictogramme_edit',));
                        }

                        // admin_terreplurielle_fo_pictogramme_delete
                        if (preg_match('#^/admin/terreplurielle/fo/pictogramme/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_pictogramme_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.pictogramme',  '_sonata_name' => 'admin_terreplurielle_fo_pictogramme_delete',));
                        }

                        // admin_terreplurielle_fo_pictogramme_show
                        if (preg_match('#^/admin/terreplurielle/fo/pictogramme/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_pictogramme_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.pictogramme',  '_sonata_name' => 'admin_terreplurielle_fo_pictogramme_show',));
                        }

                        // admin_terreplurielle_fo_pictogramme_export
                        if ($pathinfo === '/admin/terreplurielle/fo/pictogramme/export') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.pictogramme',  '_sonata_name' => 'admin_terreplurielle_fo_pictogramme_export',  '_route' => 'admin_terreplurielle_fo_pictogramme_export',);
                        }

                    }

                    if (0 === strpos($pathinfo, '/admin/terreplurielle/fo/phrase')) {
                        if (0 === strpos($pathinfo, '/admin/terreplurielle/fo/phraseproposee')) {
                            // admin_terreplurielle_fo_phraseproposee_list
                            if ($pathinfo === '/admin/terreplurielle/fo/phraseproposee/list') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.phraseproposee',  '_sonata_name' => 'admin_terreplurielle_fo_phraseproposee_list',  '_route' => 'admin_terreplurielle_fo_phraseproposee_list',);
                            }

                            // admin_terreplurielle_fo_phraseproposee_create
                            if ($pathinfo === '/admin/terreplurielle/fo/phraseproposee/create') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.phraseproposee',  '_sonata_name' => 'admin_terreplurielle_fo_phraseproposee_create',  '_route' => 'admin_terreplurielle_fo_phraseproposee_create',);
                            }

                            // admin_terreplurielle_fo_phraseproposee_batch
                            if ($pathinfo === '/admin/terreplurielle/fo/phraseproposee/batch') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.phraseproposee',  '_sonata_name' => 'admin_terreplurielle_fo_phraseproposee_batch',  '_route' => 'admin_terreplurielle_fo_phraseproposee_batch',);
                            }

                            // admin_terreplurielle_fo_phraseproposee_edit
                            if (preg_match('#^/admin/terreplurielle/fo/phraseproposee/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_phraseproposee_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.phraseproposee',  '_sonata_name' => 'admin_terreplurielle_fo_phraseproposee_edit',));
                            }

                            // admin_terreplurielle_fo_phraseproposee_delete
                            if (preg_match('#^/admin/terreplurielle/fo/phraseproposee/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_phraseproposee_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.phraseproposee',  '_sonata_name' => 'admin_terreplurielle_fo_phraseproposee_delete',));
                            }

                            // admin_terreplurielle_fo_phraseproposee_show
                            if (preg_match('#^/admin/terreplurielle/fo/phraseproposee/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_phraseproposee_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.phraseproposee',  '_sonata_name' => 'admin_terreplurielle_fo_phraseproposee_show',));
                            }

                            // admin_terreplurielle_fo_phraseproposee_export
                            if ($pathinfo === '/admin/terreplurielle/fo/phraseproposee/export') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.phraseproposee',  '_sonata_name' => 'admin_terreplurielle_fo_phraseproposee_export',  '_route' => 'admin_terreplurielle_fo_phraseproposee_export',);
                            }

                        }

                        if (0 === strpos($pathinfo, '/admin/terreplurielle/fo/phrasecomposee')) {
                            // admin_terreplurielle_fo_phrasecomposee_list
                            if ($pathinfo === '/admin/terreplurielle/fo/phrasecomposee/list') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.phrasecomposee',  '_sonata_name' => 'admin_terreplurielle_fo_phrasecomposee_list',  '_route' => 'admin_terreplurielle_fo_phrasecomposee_list',);
                            }

                            // admin_terreplurielle_fo_phrasecomposee_create
                            if ($pathinfo === '/admin/terreplurielle/fo/phrasecomposee/create') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.phrasecomposee',  '_sonata_name' => 'admin_terreplurielle_fo_phrasecomposee_create',  '_route' => 'admin_terreplurielle_fo_phrasecomposee_create',);
                            }

                            // admin_terreplurielle_fo_phrasecomposee_batch
                            if ($pathinfo === '/admin/terreplurielle/fo/phrasecomposee/batch') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.phrasecomposee',  '_sonata_name' => 'admin_terreplurielle_fo_phrasecomposee_batch',  '_route' => 'admin_terreplurielle_fo_phrasecomposee_batch',);
                            }

                            // admin_terreplurielle_fo_phrasecomposee_edit
                            if (preg_match('#^/admin/terreplurielle/fo/phrasecomposee/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_phrasecomposee_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.phrasecomposee',  '_sonata_name' => 'admin_terreplurielle_fo_phrasecomposee_edit',));
                            }

                            // admin_terreplurielle_fo_phrasecomposee_delete
                            if (preg_match('#^/admin/terreplurielle/fo/phrasecomposee/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_phrasecomposee_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.phrasecomposee',  '_sonata_name' => 'admin_terreplurielle_fo_phrasecomposee_delete',));
                            }

                            // admin_terreplurielle_fo_phrasecomposee_show
                            if (preg_match('#^/admin/terreplurielle/fo/phrasecomposee/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_phrasecomposee_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.phrasecomposee',  '_sonata_name' => 'admin_terreplurielle_fo_phrasecomposee_show',));
                            }

                            // admin_terreplurielle_fo_phrasecomposee_export
                            if ($pathinfo === '/admin/terreplurielle/fo/phrasecomposee/export') {
                                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.phrasecomposee',  '_sonata_name' => 'admin_terreplurielle_fo_phrasecomposee_export',  '_route' => 'admin_terreplurielle_fo_phrasecomposee_export',);
                            }

                        }

                    }

                }

                if (0 === strpos($pathinfo, '/admin/terreplurielle/fo/theme')) {
                    // admin_terreplurielle_fo_theme_list
                    if ($pathinfo === '/admin/terreplurielle/fo/theme/list') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.theme',  '_sonata_name' => 'admin_terreplurielle_fo_theme_list',  '_route' => 'admin_terreplurielle_fo_theme_list',);
                    }

                    // admin_terreplurielle_fo_theme_create
                    if ($pathinfo === '/admin/terreplurielle/fo/theme/create') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.theme',  '_sonata_name' => 'admin_terreplurielle_fo_theme_create',  '_route' => 'admin_terreplurielle_fo_theme_create',);
                    }

                    // admin_terreplurielle_fo_theme_batch
                    if ($pathinfo === '/admin/terreplurielle/fo/theme/batch') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.theme',  '_sonata_name' => 'admin_terreplurielle_fo_theme_batch',  '_route' => 'admin_terreplurielle_fo_theme_batch',);
                    }

                    // admin_terreplurielle_fo_theme_edit
                    if (preg_match('#^/admin/terreplurielle/fo/theme/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_theme_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.theme',  '_sonata_name' => 'admin_terreplurielle_fo_theme_edit',));
                    }

                    // admin_terreplurielle_fo_theme_delete
                    if (preg_match('#^/admin/terreplurielle/fo/theme/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_theme_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.theme',  '_sonata_name' => 'admin_terreplurielle_fo_theme_delete',));
                    }

                    // admin_terreplurielle_fo_theme_show
                    if (preg_match('#^/admin/terreplurielle/fo/theme/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_terreplurielle_fo_theme_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.theme',  '_sonata_name' => 'admin_terreplurielle_fo_theme_show',));
                    }

                    // admin_terreplurielle_fo_theme_export
                    if ($pathinfo === '/admin/terreplurielle/fo/theme/export') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.theme',  '_sonata_name' => 'admin_terreplurielle_fo_theme_export',  '_route' => 'admin_terreplurielle_fo_theme_export',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/admin/sonata/user')) {
                if (0 === strpos($pathinfo, '/admin/sonata/user/user')) {
                    // admin_sonata_user_user_list
                    if ($pathinfo === '/admin/sonata/user/user/list') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_sonata_user_user_list',  '_route' => 'admin_sonata_user_user_list',);
                    }

                    // admin_sonata_user_user_create
                    if ($pathinfo === '/admin/sonata/user/user/create') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_sonata_user_user_create',  '_route' => 'admin_sonata_user_user_create',);
                    }

                    // admin_sonata_user_user_batch
                    if ($pathinfo === '/admin/sonata/user/user/batch') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_sonata_user_user_batch',  '_route' => 'admin_sonata_user_user_batch',);
                    }

                    // admin_sonata_user_user_edit
                    if (preg_match('#^/admin/sonata/user/user/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_user_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_sonata_user_user_edit',));
                    }

                    // admin_sonata_user_user_delete
                    if (preg_match('#^/admin/sonata/user/user/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_user_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_sonata_user_user_delete',));
                    }

                    // admin_sonata_user_user_show
                    if (preg_match('#^/admin/sonata/user/user/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_user_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_sonata_user_user_show',));
                    }

                    // admin_sonata_user_user_export
                    if ($pathinfo === '/admin/sonata/user/user/export') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.user.admin.user',  '_sonata_name' => 'admin_sonata_user_user_export',  '_route' => 'admin_sonata_user_user_export',);
                    }

                }

                if (0 === strpos($pathinfo, '/admin/sonata/user/group')) {
                    // admin_sonata_user_group_list
                    if ($pathinfo === '/admin/sonata/user/group/list') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_sonata_user_group_list',  '_route' => 'admin_sonata_user_group_list',);
                    }

                    // admin_sonata_user_group_create
                    if ($pathinfo === '/admin/sonata/user/group/create') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_sonata_user_group_create',  '_route' => 'admin_sonata_user_group_create',);
                    }

                    // admin_sonata_user_group_batch
                    if ($pathinfo === '/admin/sonata/user/group/batch') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_sonata_user_group_batch',  '_route' => 'admin_sonata_user_group_batch',);
                    }

                    // admin_sonata_user_group_edit
                    if (preg_match('#^/admin/sonata/user/group/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_group_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_sonata_user_group_edit',));
                    }

                    // admin_sonata_user_group_delete
                    if (preg_match('#^/admin/sonata/user/group/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_group_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_sonata_user_group_delete',));
                    }

                    // admin_sonata_user_group_show
                    if (preg_match('#^/admin/sonata/user/group/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_user_group_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_sonata_user_group_show',));
                    }

                    // admin_sonata_user_group_export
                    if ($pathinfo === '/admin/sonata/user/group/export') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.user.admin.group',  '_sonata_name' => 'admin_sonata_user_group_export',  '_route' => 'admin_sonata_user_group_export',);
                    }

                }

            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
